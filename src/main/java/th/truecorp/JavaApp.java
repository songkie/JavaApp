package th.truecorp;

public class JavaApp {
    public static void main(String args[]) {
        
        
        Validator validator = new Validator();
        boolean result = validator.validate();

        TemplatePopulator templatePopulator = new TemplatePopulator();

        System.out.printf("Validator result: %s\n", result? "Pass": "Failed" );

        for (int i = 0; i < 20; i++) {
            String param = templatePopulator.populate2();
            System.out.printf("Template param: %s\n", param);

        }
    }
}
