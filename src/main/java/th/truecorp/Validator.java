package th.truecorp;

import th.truecorp.core.rule.condition.IsMobile;
import th.truecorp.core.rule.condition.IsMultiSub;
import th.truecorp.data.Account;

import java.util.function.Predicate;

public class Validator {
    static Predicate<Account> r1 = new IsMobile();
    static Predicate<Account> r2 = new IsMultiSub();
    
    public boolean validate() {
        

        
        boolean checkMobile = true;
        boolean checkMultiSub = false;
        
        Account acc = new Account("RM", 1);
        
        
        // Validator 
        Predicate<Account> combinedRules = null;
        
        if (checkMobile) {
            combinedRules = r1;
        }

        if (checkMultiSub) {
            combinedRules = combinedRules.and(r2);
        }


        return combinedRules.test(acc);
    }
}
