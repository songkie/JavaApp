package th.truecorp.data;

public class Account
{
    private String companyCode;
    private Integer noOfSub;

    public Account(String companyCode, Integer noOfSub) {
        this.companyCode = companyCode;
        this.noOfSub = noOfSub;
    }

    public Integer getNoOfSub() {
        return this.noOfSub;
    }

    public String getCompanyCode() {
        return this.companyCode;
    }
}