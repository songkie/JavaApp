package th.truecorp.core.rule.condition;

import th.truecorp.data.Account;

import java.util.function.Function;
import java.util.function.Predicate;

public class IsMobile implements Predicate<Account> {
    
    @Override
    public boolean test(Account account) {
        if (account.getCompanyCode().equals("RM"))
            return true;
        else
            return false;
    }
}