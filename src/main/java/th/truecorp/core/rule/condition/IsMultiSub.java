package th.truecorp.core.rule.condition;

import th.truecorp.data.Account;

import java.util.function.Function;
import java.util.function.Predicate;

public class IsMultiSub implements Predicate<Account>
{
    @Override
    public boolean test(Account account) {
        if (account.getNoOfSub() > 1 )
            return true;
        else
            return false;
    }
    
}