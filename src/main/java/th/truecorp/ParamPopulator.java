package th.truecorp;

import com.jayway.jsonpath.JsonPath;
import th.truecorp.data.Account;
import th.truecorp.data.Expression;

import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParamPopulator implements Function<Function<String, String>, Function<Map<String,Object>, String>> {

    final String regex = "\\$\\{([A-Za-z0-9.]+)}";
    
    private int nArgs = 0;
    
    @Override
    public Function<Map<String,Object>, String> apply(Function<String, String> f) {
        
            return (map) -> {

                String expr = f.apply("");

                Pattern VAR_PATTERN = Pattern.compile(regex);
                Matcher m = VAR_PATTERN.matcher(expr);

                String varName = "";
                while (m.find()) {
                    varName = m.group(1);
                }

                String[] paths = varName.split("\\.");
                
                String value = "";
                Map subMap = map;

                boolean notFound = false;
                for (int i = 0; i < paths.length; i++) {

                    String path = paths[i];
                    
                    if (i < paths.length - 1) {
                        subMap = (Map) subMap.get(path);
                    } else {
                        value = subMap.get(path).toString();
                    }
                    
                    if (subMap == null || value == null) {
                        notFound = true;
                        break;
                    }
                }
                
                if (notFound == false) {

                    return expr.replaceFirst(regex, value);
                } else {
                    return "";
                }
            };
        
    }
}


