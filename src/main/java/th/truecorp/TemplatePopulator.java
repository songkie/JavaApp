package th.truecorp;

import com.fasterxml.jackson.core.JsonProcessingException;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;

import com.fasterxml.jackson.databind.ObjectMapper;


public class TemplatePopulator {

    final String regex = "\\$\\{[A-Za-z0-9]+}";
    Map<String,Object> jsonObj;
    
    Function fx = null;
    int fx_nArgs = 0;
    List<Function> vars;

    private Function<Function<String, String>, Function<Map<String,Object>, String>>  p1 = new ParamPopulator();
    
    public TemplatePopulator() {
        parser();
    }
    
    public String populate() {
 
        Map<String, String> vars = new HashMap<String, String>() {{
            put("voice.amount", "95.00");
            put("sms.amount", "10.00");
            put("data.amount", "200.24");
        }};

        // Parser Step (one-time)
        String var1 = "ค่าใช้จ่ายส่วนเกิน ${voice} / ${sms} / ${data}";
        String voice = "ค่าโทร = ${voice.amount}";
        String sms = "ข้อความ = ${sms.amount}";
        String data = "ดาต้า = ${sms.amount}";
        
        // Construction Step (one-time)
        Function<Map<String,Object>, String> f1 = p1.apply(s -> "ค่าโทร = {}");
        Function<Map<String,Object>, String> f2 = p1.apply(s -> "ข้อความ = {}");
        Function<Map<String,Object>, String> f3 = p1.apply(s -> "ดาต้า = {}");

        Function<String,
                Function<String,
                        Function<String, String> > > 
                f = 
                
                
                s1 -> s2 -> s3 ->
            "ค่าใช้จ่ายส่วนเกิน {} / {} / {}"
                    .replaceFirst("\\{}", s1)
                    .replaceFirst("\\{}", s2)
                    .replaceFirst("\\{}", s3);
        
        
        
        // SMS Bill Step (loop for each SMS)

        String param =  f.apply(f1.apply(jsonObj))
                         .apply(f2.apply(jsonObj))
                         .apply(f3.apply(jsonObj));
        
        
        return param;
    }
    
    private void parser() {
        // Parser Step (one-time)
        String var1 = "ค่าใช้จ่ายส่วนเกิน ${voice} / ${sms} / ${data}";
        String voice = "ค่าโทร = ${voice.amount}";
        String sms = "ข้อความ = ${sms.amount}";
        String data = "ดาต้า = ${data.amount}";

        // Construction Step (one-time)
        Function<Map<String,Object>, String> v1 = p1.apply(s -> voice);
        Function<Map<String,Object>, String> v2 = p1.apply(s -> sms);
        Function<Map<String,Object>, String> v3 = p1.apply(s -> data);




        this.vars = List.of(v1, v2, v3);


        int nArgs = var1.split(regex).length;


        Function f = null;

        switch (nArgs)
        {
            case 0:
                break;
            case 1:
                Function<String, String> f1 = (s) -> var1.replaceFirst(regex, s);

                f = f1;
                break;
            case 2:
                Function<String,
                        Function<String, String> >
                        f2 = s1 -> s2 -> var1.replaceFirst(regex, s1)
                        .replaceFirst(regex, s2);

                f = f2;
                break;
            case 3:
                Function<String,
                        Function<String,
                                Function<String, String> > >
                        f3 = s1 -> s2 -> s3 -> var1.replaceFirst(regex, s1)
                        .replaceFirst(regex, s2)
                        .replaceFirst(regex, s3);

                f = f3;
                break;

        }

        this.fx = f;
        this.fx_nArgs = nArgs;
    }
    
    
    private void fetchData() {
        ObjectMapper mapper = new ObjectMapper();

        Random r = new Random();
        double random1 = 100.00 + r.nextDouble() * (100.00 - 0.00);
        double random2 = 100.00 + r.nextDouble() * (100.00 - 0.00);
        double random3 = 100.00 + r.nextDouble() * (100.00 - 0.00);
        
        String json1 = "{\n" +
                "\t\"voice\": {\n" +
                "\t\t\"amount\": " + String.format( "%.2f",random1) + "\n" +
                "\t},\n" +
                "\t\"sms\": {\n" +
                "\t\t\"amount\": " + String.format( "%.2f",random2) + "\n" +
                "\t},\n" +
                "\t\"data\": {\n" +
                "\t    \"amount\": " + String.format( "%.2f",random3) + "\n" +
                "\t}\n" +
                "}";

        String json2 = "{\n" +
                "\t\"voice\": {\n" +
                "\t\t\"amount\": " + String.format( "%.2f",random1) + "\n" +
                "\t},\n" +
                "\t\"sms\": {\n" +
                "\t\t\"amount\": " + String.format( "%.2f",random2) + "\n" +
                "\t}\n" +
                "}";

        String json3 = "{\n" +
                "\t\"voice\": {\n" +
                "\t\t\"amount\": " + String.format( "%.2f",random1) + "\n" +
                "\t},\n" +
                "\t\"data\": {\n" +
                "\t    \"amount\": " + String.format( "%.2f",random3) + "\n" +
                "\t}\n" +
                "}";

        String json4 = "{\n" +
                "\t\"voice\": {\n" +
                "\t\t\"amount\": " + String.format( "%.2f",random1) + "\n" +
                "\t}\n" +
                "}";

        String json5 = "{\n" +
                "\t\"data\": {\n" +
                "\t\t\"amount\": " + String.format( "%.2f",random1) + "\n" +
                "\t}\n" +
                "}";

        String json6 = "{\n" +
                "\t\"sms\": {\n" +
                "\t\t\"amount\": " + String.format( "%.2f",random1) + "\n" +
                "\t}\n" +
                "}";
        
        String[] jsonArray = new String[] { json1, json2, json3, json4, json5, json6 };

        int ramdomJsonIndex = Math.abs(r.nextInt()) % jsonArray.length;


        jsonObj = null;
        try {
            jsonObj = mapper.readValue(jsonArray[ramdomJsonIndex], Map.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
    
    public String populate2() {

        fetchData();
        
        String result = "";
        
        Function f = this.fx;
        for (int i = 0; i < fx_nArgs; i++) {

            if (i < fx_nArgs - 1) {
                f = (Function) f.apply(vars.get(i).apply(jsonObj));
            } else {
                result = (String) f.apply(vars.get(i).apply(jsonObj));
            }
        }
        return  result;
    }
}
